const express = require('express')
const app = express()

global.connection = require('./helpers/database')
global.Sequelize = require('sequelize')

const helmet = require('helmet')
app.use(helmet())

const dotenv = require('dotenv')
const envfile = '.env'

dotenv.config( {path: envfile})

const cors = require('cors')
app.use(cors())

const bodyParser = require('body-parser')
app.use(bodyParser.json({limit:'60mb'}))
app.use(bodyParser.urlencoded({limit: '60mb' , extended: true}))

app.use('/api/v1',require('./routes/v1'))

app.use(require('./helpers/response.helper'))

app.use(require('./helpers/error.helper').handleValidationErrors)

app.use(require('./helpers/error.helper').handleErrors)

const port = process.env.port || 3000

app.listen(port, () => {
    console.log("Listening port", port)
})