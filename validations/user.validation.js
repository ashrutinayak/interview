const Joi = require('@hapi/joi')

module.exports = {
    signup: Joi.object({
        first_name: Joi.string().required().empty().message({
            'string.base':'First Name should be string',
            'string.empty':'First Name can not be an empty field',
            'any.required':'First Name is a required field'
        }),
        last_name: Joi.string().required().empty().message({
            'string.base':'Last Name should be string',
            'string.empty':'Last Name can not be an empty field',
            'any.required':'Last Name is a required field'
        }),
        role: Joi.number().required().message({
            'any.required':'Role is a required field'
        }),
        email: Joi.string().required().empty().email().message({
            'string.base':'Email should be string',
            'string.empty':'Email can not be an empty field',
            'string.email':'Email formate not valid',
            'any.required':'Email is a required field'
        }),
        password: Joi.string().required().empty().regex(/^[A-Za-z0-9_@./#&+-]*$/).min(6).max(16).message({
            'string.base':'password should be string',
            'string.empty':'password can not be an empty field',
            'string.min':'password should be of minimum 6 characters',
            'string.max':'password should be of maximum 16 characters',
            'string.pattern.base':'Password must contains lower case, upper case and digit',
            'any.required':'password is a required field'
        }),
        date_of_birth: Joi.string().required().empty().message({
            'string.base':'Last Name should be string',
            'string.empty':'Last Name can not be an empty field',
            'any.required':'Last Name is a required field'
        })
    }),
    signin: Joi.object({
        email: Joi.string().required().empty().email().message({
            'string.base':'Email should be string',
            'string.empty':'Email can not be an empty field',
            'string.email':'Email formate not valid',
            'any.required':'Email is a required field'
        }),
        password: Joi.string().required().empty().regex(/^[A-Za-z0-9_@./#&+-]*$/).min(6).max(16).message({
            'string.base':'password should be string',
            'string.empty':'password can not be an empty field',
            'string.min':'password should be of minimum 6 characters',
            'string.max':'password should be of maximum 16 characters',
            'string.pattern.base':'Password must contains lower case, upper case and digit',
            'any.required':'password is a required field'
        })
    })
}