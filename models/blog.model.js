const blogSchema = require('../migrations/blogs')
const user = require('../migrations/user')
const category = require('../migrations/category')

exports.create = async (data, callback) => {
    let blogdata = {
        title: data.title,
        description: data.description,
        category: data.category,
        author: data.author,
        publish_date: data.publish_date,
        modify_date: data.modify_date,
        start_date: data.start_date,
        end_date: data.end_date,
        status: data.status,
        recurrence: data.recurrence
    }

    let blog = await blogSchema.create(blogdata)
    callback(blog)
}

exports.list = async (req, callback) => {
    let currentPage = parseInt(req.currentPage) || 1
    let limit = parseInt(req.limit) || 10
    let offset = (currentPage - 1) * limit
    let sortBy = req.sortBy || 'id'
    let sortType = req.sortType || 'DESC'
    let search = req.search || ''
    let userRole = req.decoded.role
    let userId = req.decoded.userId
    let blog = [];
    if (userRole == 1) {
        blog = await blogSchema.findAndCountAll({
            where:
            {
                [Sequelize.Op.or]: [{
                    title: {
                        [Sequelize.Op.like]: '%' + search + '%'
                    },
                    publish_date: {
                        [Sequelize.Op.like]: '%' + search + '%'
                    }
                }
                ]
            },
            order:[
                sortBy,sortType
            ],
            offset,
            limit,
            include: [{
                model: user,
                where: {
                    [Sequelize.Op.or]: [{
                        first_name: '%' + search + '%',
                    },
                    { last_name: '%' + search + '%' }]
                }
            },
            {
                model: category,
                where: {
                    [Sequelize.Op.or]: [{
                        name: '%' + search + '%'
                    }]
                }
            }]
        })
    } else if (userRole == 2) {
        blog = await blogSchema.findAndCountAll({
            where:
            {
                author: userId,
                [Sequelize.Op.or]: [{
                    title: {
                        [Sequelize.Op.like]: '%' + search + '%'
                    },
                    publish_date: {
                        [Sequelize.Op.like]: '%' + search + '%'
                    }
                }
                ]
            },
            include: [{
                model: user,
                where: {
                    [Sequelize.Op.or]: [{
                        first_name: '%' + search + '%',
                    },
                    { last_name: '%' + search + '%' }]
                }
            },
            {
                model: category,
                where: {
                    [Sequelize.Op.or]: [{
                        name: '%' + search + '%'
                    }]
                }
            }]
        })
    }
    if (blog) {
        callback(blog)
    }
    callback('')
}

exports.getById = async (id, callback) => {
    let blog = await blogSchema.findOne({
        where: {
            id: id
        }
    })
    if (blog) {
        callback(blog)
    }
    callback('')
}
exports.update = async (data, callback) => {
    let blogdata = {
        title: data.title,
        description: data.description,
        category: data.category,
        author: data.author,
        publish_date: data.publish_date,
        modify_date: data.modify_date,
        start_date: data.start_date,
        end_date: data.end_date,
        status: data.status,
        recurrence: data.recurrence
    }
    let id = data.id
    let blog = await blogSchema.update(blogdata, {
        where: {
            id: id
        }
    })
    callback(blog)
}

exports.delete = async (id, callback) => {
    let blog = await blogSchema.destroy({
        where: { id }
    })
    callback(blog)
}