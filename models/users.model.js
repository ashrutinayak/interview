const userSchema = require('../migrations/user')

exports.sigup = async(data, callback) => {
    let userdata = {
        first_name: data.first_name,
        last_name: data.last_name,
        email:data.email,
        password:data.password,
        role:data.role,
        date_of_birth:data.date_of_birth
    }

    let user = await userSchema.create(userdata)
    callback(user)
}

exports.isUserExistEmail = async( email, callback) => {
    let user = await userSchema.findOne({
        where:{
            email:email
        }
    })
    if(user){
        callback(user)
    }
    callback('')
}