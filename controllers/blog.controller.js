const { GeneralError, UnAuthorized } = require('../utils/error')
const { GeneralResponse } = require('../utils/response')
const blogModel = require('../models/blog.model')
const config = require('../utils/config')

exports.create = async (req, res, next) => {
    try {
        const { title, description, category, author, publish_date, modify_date ,start_date , end_date ,status, recurrence } = req.body
        blogModel.create({ title, description, category, author, publish_date, modify_date ,start_date , end_date ,status, recurrence }, async (userRes) => {
            if (userRes) {
                next(new GeneralResponse('Blog Add Successfully.'))
            }
            else {
                next(new GeneralError("Something was wrong."))
            }
        })

    } catch (err) {
        next(new GeneralError('blog Add Failed'))
    }
}

exports.list = async (req, res, next) => {
    try {
        await blogModel.list(req, async (response) => {
            if (response) {
                    new GeneralResponse("Blog List successfully.", response)
            }
            next(new UnAuthorized("Blog listing error"))
        })
    } catch (err) {
        next(new UnAuthorized("Blog listing failed."))
    }
}

exports.update = async (req, res, next) => {
    try {
        let id = req.params.id
        const { title, description, category, author, publish_date, modify_date ,start_date , end_date ,status, recurrence } = req.body
        blogModel.getById(id, async(response) => {
            if(response){
                blogModel.update({ title, description, category, author, publish_date, modify_date ,start_date , end_date ,status, recurrence , id }, async (blogRes) => {
                    if (blogRes) {
                        next(new GeneralResponse('Blog Update Successfully.'))
                    }
                    else {
                        next(new GeneralError("Something was wrong."))
                    }
                })
            }else{
                next(new GeneralError("blog not found."))
            }
        })

    } catch (err) {
        next(new GeneralError('blog update Failed'))
    }
}

exports.delete = async (req, res, next) => {
    try {
        let id = req.params.id
        blogModel.getById(id, async(response) => {
            if(response){
                blogModel.delete( id, async (blogRes) => {
                    if (blogRes) {
                        next(new GeneralResponse('Blog delete Successfully.'))
                    }
                    else {
                        next(new GeneralError("Something was wrong."))
                    }
                })
            }else{
                next(new GeneralError("blog not found."))
            }
        })

    } catch (err) {
        next(new GeneralError('blog update Failed'))
    }
}