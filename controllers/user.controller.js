const { GeneralError ,UnAuthorized } = require('../utils/error')
const { GeneralResponse } = require('../utils/response')
const userModels = require('../models/users.model')
const config = require('../utils/config')
const bcrypt = require('bcrypt')
const { generateToken } = require('../helpers/auth.helper')

exports.signup = async (req, res, next) =>{
    try{
        const {first_name,last_name,email,password,date_of_birth,role} = req.body
        userModels.isUserExistEmail(email, async( response) => {
            if(!response){
                userModels.sigup({first_name,last_name,email,password,date_of_birth,role},async(userRes) => {
                    if(userRes){
                        next(new GeneralResponse('User Successfully Sign Up.'))
                    }
                    else{
                        next( new GeneralError("Something was wrong."))
                    }
                })
            }
        })
    }catch(err){
        next(new GeneralError('User Sign Up Failed'))
    }
}
exports.signin = async (req, res, next) => {
    try {
        const { email , password } = req.body
        await userModels.isUserExistEmail(email, async(response) => {
            if(response) {
                const comparision = await bcrypt.compare(password,response.password)
                if(comparision){
                    let userdata = {
                        id: response.id,
                        role: response.role
                    }
                    let token = await generateToken(userdata)
                    new GeneralResponse("login successfully.",{
                        token:token,
                        email:response.email,
                        role:response.role,
                        first_name: response.first_name,
                        last_name: response.last_name
                    },config.HTTP_SUCCESS)
                }
                next(new UnAuthorized("Password does not match."))
            }
            next(new UnAuthorized("email address does not match"))
        })
    }catch(err){
        next( new GeneralError("User Login failed."))
    }
}