const Blogs = connection.database.define('blogs', {
    id: {
        type: Sequelize.BIGINT(20).UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING(255),
        default: ""
    },
    description: {
        type: Sequelize.STRING(255),
        default: ""
    },
    category: {
        type: Sequelize.BIGINT(255),
    },
    author: {
        type: Sequelize.BIGINT(255),
    },
    publish_date: {
        type: Sequelize.DATEONLY
    },
    modify_date: {
        type: Sequelize.DATEONLY
    },
    start_date: {
        type: Sequelize.DATEONLY
    },
    end_date: {
        type: Sequelize.DATEONLY
    },
    status: {
        type: Sequelize.BIGINT(255),
        default: 0,
        comment: "0 =>UnPublish , 2 => Publish"
    },
    recurrence: {
        type: Sequelize.JSON()
    }
}, {
    underscored: true,

})
let user = require('./user')
let category = require('./category')
Blogs.associate = (models) => {
    models.category.hasOne(Blogs, {
        foreignKey: "category",
        onDelete: 'cascade'
    })

    Blogs.hasOne(models.category, {
        foreignKey: "category",
        onDelete: 'cascade'
    })

    models.user.hasMany = (Blogs, {
        foreignKey: "author",
        onDelete: 'cascade'
    })

    Blogs.hasOne(models.user, {
        foreignKey: "author",
        onDelete: 'cascade'
    })
}

Blogs.sync({
    logging: false
})

module.exports = Blogs