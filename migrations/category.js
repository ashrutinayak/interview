
const Categories = connection.database.define('categories',{
    id:{
        type: Sequelize.BIGINT(20).UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING(255),
        default:""
    }
},{
    underscored:true,
})

Categories.sync({
    logging:false
})

module.exports = Categories