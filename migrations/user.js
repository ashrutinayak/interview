const saltRound = 10
const bcrypt = require('bcrypt')
const Users = connection.database.define('users',{
    id:{
        type: Sequelize.BIGINT(20).UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    first_name: {
        type: Sequelize.STRING(255),
        default:""
    },
    last_name:{
        type: Sequelize.STRING(255),
        default:""
    },
    email: {
        type:Sequelize.STRING(255),
        unique: true,
        set(val) {
            this.setDataValue('email',val.toLowerCase())
        }
    },
    password:{
        type: Sequelize.STRING(255),
        default:""
    },
    role: {
        type: Sequelize.BIGINT(255),
        default:"",
        comment: "1 =>Admin , 2 => User"
    },
    date_of_birth: {
        type: Sequelize.DATEONLY
    }
},{
    underscored:true,
    hooks: {
        beforeCreate: async (user,option) => {
            user.password = await bcrypt.hash(user.password, saltRound);
        }
    }
})

Users.sync({
    logging:false
})

module.exports = Users