const jwt = require('jsonwebtoken')
const unAuthorized = require('../utils/error')

const authenticate = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']

    if(token){
        if(token.startsWith('Bearer')){
            token = token.slice(7,token.length)
        }
        jwt.verify(token,process.env.SECRET_KEY,(err, decoded) => {
            if(err)
                next(new unAuthorized.UnAuthorized('auth token is invalid'))
            else{
                req.decoded = decoded
                next();
            }
        })
    }else{
        next(new unAuthorized.UnAuthorized('auth token not supplied '))
    }
}

const generateToken = (data) =>{
    const token = jwt.sign(data,process.env.SECRET_KEY,{
        expiresIn : process.env.TOKEN_EXPIRY
    })
}

module.exports = {
    generateToken,authenticate
}