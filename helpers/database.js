require('dotenv').config()

const Sequelize = require('sequelize')

const connection = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        host:process.env.DB_HOST,
        port:process.env.DB_PORT,
        dialect:'mysql',
        logging:false
    }
)

connection.authenticate().then( ()=> {
    console.log("connection established successfully.")
}).catch((err) => {
    console.log("failed database connection" , err)
})

module.exports = {
    database: connection
}