const { GeneralError , BadRequest } = require('../utils/error')
const cofig = require('../utils/config')

const handleErrors = (err, req, res, next) => {
    if(err instanceof GeneralError) {
        return res.status(err.statusCode !== '' ? err.statusCode : err.getCode()).json({
            status:config.ERROR,
            code:err.statusCode !== '' ? err.statusCode : err.getCode(),
            message:response.message,
            result:err.result !== '' ? err.result : undefined
        })
    }

    return res.status(config.HTTP_SERVER_ERROR).json({
        status:config.ERROR,
        code:config.HTTP_SERVER_ERROR,
        message:response.message
    })
}

const handleValidationErrors = (err, req, res, next) => {
    if(err && err.error && err.error.isJoi){

        const customErrorResponse = {}

        if(err.error.details.length !== 0) {
            err.error.details.forEach(item => {
                customErrorResponse[`${item.context.key}`] = {
                    message:item.message,
                    context:item.context.label,
                    type:item.type
                }
            })
        }
        next(new BadRequest('validation error',customErrorResponse))
    }
    else if(err.name == 'SequelizeValidationError'){
        const response = {}
        
        err.error.forEach(item => {
            response[`${item.path}`] = {
                message: item.message,
                type: item.type
            }
        })

        next(new BadRequest('SequelizeValidation Error',response))
    }
    else{
        next(err)
    }
}

module.exports = {
    handleErrors,handleValidationErrors
}