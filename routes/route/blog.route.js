const express = require('express')
const router = express.Router()
const blogController = require('../../controllers/blog.controller')
const  { validator } = require('../../helpers/validatore.helper')
const { authenticate } = require("../../helpers/auth.helper")
// const blogValidation = require('../../validations/blog.validation')

router.post('/create',authenticate,blogController.create)
router.post('/list',authenticate,blogController.list)
router.put('/update/:id',authenticate,blogController.update)
router.delete('/delete/:id',authenticate,blogController.delete)

module.exports = router