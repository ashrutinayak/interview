const express = require('express')
const router = express.Router()
const userController = require('../../controllers/user.controller')
const  { validator } = require('../../helpers/validatore.helper')
// const userValidation = require('../../validations/user.validation')

router.post('/signUp',userController.signup)
router.post('/signin',userController.signin)

module.exports = router