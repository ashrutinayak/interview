let express = require('express')
let router = express.Router()

const user = require('./route/user.route')
const blog = require('./route/blog.route')

router.use('/user',user)
router.use('/blog',blog)

module.exports = router